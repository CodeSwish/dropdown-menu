import "./style.css";

// modular style
const dropDownMenu = (function () {
  let childList;

  const init = () => {
    render();
  };

  const cacheDom = () => {
    childList = document.querySelectorAll("[data-child-list-item]");
  };

  const loopMenu = (elMenu) => {
    elMenu.forEach((menu) => {
      bindEvents(menu);
    });
  };

  const bindEvents = (elMenu) => {
    elMenu.addEventListener("click", () =>
      showSubMenu(elMenu, "show-grandchild-list"),
    );
  };

  const showSubMenu = (menu, addedClass) => {
    menu.classList.toggle(addedClass);
  };

  const render = () => {
    cacheDom();
    loopMenu(childList);
  };

  return { init };
})();
dropDownMenu.init();

// spagetti code
// const childList = document.querySelectorAll("[data-child-list-item]");
//
// const loopMenu = (menus) => {
//   return menus.forEach((menu) => {
//     menu.addEventListener("click", showSubMenu('show-grandchild-list'));
//   });
// };
//
// childList.forEach((child) => {
//   child.addEventListener("click", function () {
//     this.classList.toggle("show-grandchild-list");
//   });
// });
//
// const showSubMenu = (menu, addedClass) => {
//   return menu.classList.toggle(addedClass);
// };
